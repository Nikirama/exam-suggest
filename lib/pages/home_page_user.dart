import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:localstore/localstore.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_flutter/qr_flutter.dart';

class HomePageUser extends StatefulWidget {
  const HomePageUser({
    Key? key,
    required this.isAdmin,
    required this.deviceId,
    required this.localStore,
  }) : super(key: key);

  final bool isAdmin;
  final String deviceId;
  final Localstore localStore;

  @override
  _HomePageUserState createState() => _HomePageUserState();
}

class _HomePageUserState extends State<HomePageUser> {
  static const localStoreSuggestsCollection = 'suggests';

  late String _newSuggestTitle;
  late String _newSuggestDescription;

  late bool isDeleteMode = false;
  late bool _autocompleteSuggestDescription = true;
  final TextEditingController _newSuggestTitleController =
      TextEditingController();
  final TextEditingController _newSuggestDescriptionController =
      TextEditingController();

  final flutterTts = FlutterTts();

  late bool _isQrScanned = false;
  Barcode? qrScanResult;
  QRViewController? qrScanController;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  void _onChangeNewSuggestTitle() {
    if (_autocompleteSuggestDescription) {
      setState(() {
        _newSuggestDescriptionController.text = _newSuggestTitleController.text;
        _newSuggestDescription = _newSuggestTitleController.text;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    _newSuggestTitleController.addListener(_onChangeNewSuggestTitle);

    initFlutterTTS();
  }

  void initFlutterTTS() async {
    await flutterTts.setSharedInstance(true);

    await flutterTts
        .setIosAudioCategory(IosTextToSpeechAudioCategory.playAndRecord, [
      IosTextToSpeechAudioCategoryOptions.allowBluetooth,
      IosTextToSpeechAudioCategoryOptions.allowBluetoothA2DP,
      IosTextToSpeechAudioCategoryOptions.mixWithOthers,
    ]);
  }

  void _onQRViewCreated(QRViewController controller) {
    qrScanController = controller;
    controller.scannedDataStream.listen((Barcode scanData) {
      Navigator.of(context).popUntil((route) => route.isFirst);

      if (!_isQrScanned) {
        _isQrScanned = true;

        FirebaseFirestore.instance.collection('activeDevices').add({
          'deviceId': scanData.code,
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar(),
      body: getBody(),
      floatingActionButton: getFloatingActionButton(context),
    );
  }

  AppBar getAppBar() {
    List<Widget> actions = [
      IconButton(
        icon: const Icon(Icons.delete_forever_outlined),
        onPressed: () {
          setState(() {
            isDeleteMode = !isDeleteMode;
          });
        },
      ),
    ];

    if (widget.isAdmin) {
      actions.add(IconButton(
        icon: const Icon(Icons.qr_code_scanner),
        tooltip: 'Qr code page',
        onPressed: () {
          Navigator.push(context, MaterialPageRoute<void>(
            builder: (BuildContext context) {
              return DefaultTabController(
                  initialIndex: 1,
                  length: 2,
                  child: Scaffold(
                      appBar: AppBar(
                        title: const Text('Qr code page'),
                        actions: <Widget>[
                          IconButton(
                            icon: const Icon(Icons.add_a_photo_outlined),
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute<void>(
                                builder: (BuildContext context) {
                                  return Scaffold(
                                    appBar: AppBar(
                                      title: const Text('Qr code scanner'),
                                    ),
                                    body: Center(
                                      child: QRView(
                                        key: qrKey,
                                        onQRViewCreated: _onQRViewCreated,
                                      ),
                                    ),
                                  );
                                },
                              ));
                            },
                          )
                        ],
                        bottom: const TabBar(
                          tabs: <Widget>[
                            Tab(
                              icon: Text('Android'),
                            ),
                            Tab(
                              icon: Text('IOS'),
                            ),
                          ],
                        ),
                      ),
                      body: TabBarView(
                        children: [
                          Center(
                            child: QrImage(
                              data: 'https://gitlab.com/',
                              version: QrVersions.auto,
                              size: 300.0,
                            ),
                          ),
                          Center(
                            child: QrImage(
                              data: 'https://vk.com/id512205001',
                              version: QrVersions.auto,
                              size: 300.0,
                            ),
                          ),
                        ],
                      )));
            },
          ));
        },
      ));
    }

    return AppBar(
      title: const Text('Suggests list'),
      actions: actions,
    );
  }

  Widget getBody() {
    return FutureBuilder(
      future: widget.localStore.collection(localStoreSuggestsCollection).get(),
      builder: (BuildContext context,
          AsyncSnapshot<Map<String, dynamic>?> suggests) {
        List<Widget> grid = [];

        if (!suggests.hasData) {
          return const Center(
            child: Text('Not has data'),
          );
        }

        suggests.data!.forEach((suggestId, suggestData) => {
              grid.add(Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 20.0,
                  horizontal: 10.0,
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: isDeleteMode ? Colors.red : Colors.blue,
                  ),
                  onPressed: () async {
                    if (isDeleteMode) {
                      widget.localStore
                          .collection(localStoreSuggestsCollection)
                          .doc(suggestId.split('/').last)
                          .delete();

                      setState(() {});
                    } else {
                      await runTextToSpeech(suggestData['description']);
                    }
                  },
                  child: Text(suggestData['title']),
                ),
              ))
            });

        return GridView.count(
          crossAxisCount: 3,
          children: grid,
        );
      },
    );
  }

  Widget getQrConfirm() {
    return Center(
      child: QrImage(
        data: widget.deviceId,
        version: QrVersions.auto,
        size: 300.0,
      ),
    );
  }

  FloatingActionButton getFloatingActionButton(BuildContext context) {
    return FloatingActionButton(
      tooltip: 'Add suggest',
      onPressed: () {
        showDialog(
          context: context,
          builder: (context) {
            return getCreateSuggestDialog(context);
          },
        );
      },
      child: const Icon(Icons.add),
    );
  }

  Widget getCreateSuggestDialog(BuildContext context) {
    return AlertDialog(
      title: const Text('Create suggest'),
      content: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextField(
              controller: _newSuggestTitleController,
              decoration: const InputDecoration(
                hintText: 'Suggest title',
              ),
              onChanged: (String value) {
                setState(() {
                  _newSuggestTitle = value;
                });
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: TextField(
                controller: _newSuggestDescriptionController,
                decoration: const InputDecoration(
                  hintText: 'Suggest description',
                ),
                onChanged: (String value) {
                  if (_autocompleteSuggestDescription) {
                    setState(() {
                      _autocompleteSuggestDescription = false;
                    });
                  }

                  _newSuggestDescription = value;
                },
              ),
            )
          ],
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () {
            final id = widget.localStore
                .collection(localStoreSuggestsCollection)
                .doc()
                .id;

            widget.localStore
                .collection(localStoreSuggestsCollection)
                .doc(id)
                .set({
              'title': _newSuggestTitle,
              'description': _newSuggestDescription,
            });

            _newSuggestTitle = '';
            _newSuggestDescription = '';
            _newSuggestTitleController.text = '';
            _newSuggestDescriptionController.text = '';

            Navigator.of(context).pop();
          },
          child: const Text('Add'),
        ),
      ],
    );
  }

  Future<void> runTextToSpeech(String value) async {
    FlutterTts flutterTts = FlutterTts();
    await flutterTts.setLanguage('ru-RU');
    await flutterTts.setVolume(1.0);
    await flutterTts.setPitch(1.0);
    await flutterTts.isLanguageAvailable('ru-RU');
    await flutterTts.setSpeechRate(1.0);

    flutterTts.speak(value);
  }
}
