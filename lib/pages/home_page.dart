import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:exam_suggest/pages/home_page_blocked.dart';
import 'package:exam_suggest/pages/home_page_user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:loader/loader.dart';
import 'package:localstore/localstore.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.isAdmin}) : super(key: key);

  final bool isAdmin;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with LoadingMixin<HomePage> {
  static const String localStoreActivatedCollection = 'activated';
  static const String firebaseActiveDevicesCollection = 'activeDevices';

  late bool isActivity = false;

  late String _deviceId;

  final localStore = Localstore.instance;

  @override
  Future<void> load() async {
    _deviceId = await getDeviceId();
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return Container();
    }

    if (widget.isAdmin) {
      return HomePageUser(
          isAdmin: widget.isAdmin, deviceId: _deviceId, localStore: localStore);
    }

    return StreamBuilder(
        stream: localStore.collection(localStoreActivatedCollection).stream,
        builder: (BuildContext context, collection) {
          if (!collection.hasData) {
            return StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection(firebaseActiveDevicesCollection)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.data != null) {
                    for (QueryDocumentSnapshot doc in snapshot.data!.docs) {
                      if (doc.get('deviceId') == _deviceId) {
                        final id = localStore
                            .collection(localStoreActivatedCollection)
                            .doc()
                            .id;

                        localStore
                            .collection(localStoreActivatedCollection)
                            .doc(id)
                            .set({'active': true});

                        isActivity = true;
                      }
                    }
                  }

                  if (isActivity) {
                    return HomePageUser(
                      isAdmin: widget.isAdmin,
                      deviceId: _deviceId,
                      localStore: localStore,
                    );
                  }

                  return HomePageBlocked(deviceId: _deviceId);
                });
          }

          return HomePageUser(
              isAdmin: widget.isAdmin,
              deviceId: _deviceId,
              localStore: localStore);
        });
  }

  Future<String> getDeviceId() async {
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

    if (Platform.isAndroid) {
      var build = await deviceInfoPlugin.androidInfo;

      return build.androidId;
    } else if (Platform.isIOS) {
      var data = await deviceInfoPlugin.iosInfo;

      return data.identifierForVendor;
    }

    return '';
  }
}
