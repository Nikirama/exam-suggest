import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class HomePageBlocked extends StatefulWidget {
  const HomePageBlocked({Key? key, required this.deviceId}) : super(key: key);

  final String deviceId;

  @override
  _HomePageBlockedState createState() => _HomePageBlockedState();
}

class _HomePageBlockedState extends State<HomePageBlocked> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Need activate application'),
      ),
      body: Center(
        child: QrImage(
          data: widget.deviceId,
          version: QrVersions.auto,
          size: 300.0,
        ),
      ),
    );
  }
}
