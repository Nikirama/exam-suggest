import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

import 'pages/home_page.dart';
import 'package:loader/loader.dart';

void main() => runApp(const ExamSuggest());

class ExamSuggest extends StatelessWidget with StatelessLoadingMixin {
  const ExamSuggest({Key? key}) : super(key: key);

  static const String firebaseAppBlockingCollection = 'appBlocking';

  @override
  Future<void> load() async {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
  }

  @override
  Widget futureBuild(BuildContext context) {
    return Loader(
      load: () async {
        return await FirebaseFirestore.instance
            .collection(firebaseAppBlockingCollection)
            .get();
      },
      builder: (BuildContext context, QuerySnapshot<dynamic> collection) {
        if (collection.docs.isNotEmpty) {
          VideoPlayerController videoPlayerController;

          videoPlayerController = VideoPlayerController.network(
              'https://r1---sn-n8v7zns6.googlevideo.com/videoplayback?expire=1638279518&ei=_tSlYeO3EK2EvdIPhKK3kAk&ip=109.245.215.29&id=o-AIN6-HohXYSJckSS_C2gyED_mVqoPTovNGLiDLDfXu6H&itag=18&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=v1v7GhroartQbt0ZXolBOQ8G&gir=yes&clen=12856940&ratebypass=yes&dur=174.939&lmt=1627000677296239&fexp=24001373,24007246&c=WEB&txp=5530434&n=bPIOPUBD0ltJuZy8&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cgir%2Cclen%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRAIgVumq7dk1ijC-olKM8PcSg_2Qkh21xrsDk9lv52it_H8CIGRKUuL62TZPiQvi0WCyFy-U9XmR7PXD0yu3x-M-T3yC&rm=sn-uxaovgnxgcp5-cxbl7e,sn-nv4se7e&req_id=ee74b3e6dabea3ee&redirect_counter=2&cms_redirect=yes&ipbypass=yes&mh=zp&mip=46.22.56.21&mm=29&mn=sn-n8v7zns6&ms=rdu&mt=1638258308&mv=m&mvi=1&pl=20&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRAIgD6VWOLn9wQqZ2YqmVMigmURH3WwJCyB7ieEzR311kY8CICviuT9wzXQCOxQ19LBNBlT2H8rhi2dIgBHA3INy5hbN');

          if (videoPlayerController.value.isInitialized) {
            videoPlayerController.play();

            return AspectRatio(
              aspectRatio: videoPlayerController.value.aspectRatio,
              child: VideoPlayer(videoPlayerController),
            );
          }

          return Container();
        }

        return const MaterialApp(
          title: 'Exam suggests',
          home: HomePage(isAdmin: false),
        );
      },
    );
  }
}
